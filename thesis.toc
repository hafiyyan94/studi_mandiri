\contentsline {chapter}{Daftar Isi}{ii}{figure.caption.1}
\contentsline {chapter}{Daftar Gambar}{iv}{figure.caption.1}
\contentsline {chapter}{Daftar Tabel}{v}{figure.caption.1}
\contentsline {chapter}{\numberline {1}\uppercase {Latar Belakang}}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}\uppercase {Desain Sistem}}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Alur Kerja Program}{4}{section.2.1}
\contentsline {section}{\numberline {2.2}Penggunaan \textit {Metamodel}}{6}{section.2.2}
\contentsline {chapter}{\numberline {3}\uppercase {Implementasi}}{8}{chapter.3}
\contentsline {section}{\numberline {3.1}\textit {Class} XMIModel}{8}{section.3.1}
\contentsline {section}{\numberline {3.2}\textit {Class} XMIPackage}{9}{section.3.2}
\contentsline {section}{\numberline {3.3}\textit {Class} XMIClass}{10}{section.3.3}
\contentsline {section}{\numberline {3.4}\textit {Class} XMIProperty}{10}{section.3.4}
\contentsline {section}{\numberline {3.5}\textit {Class} XMIOperation}{12}{section.3.5}
\contentsline {section}{\numberline {3.6}\textit {Class} XMIParameter}{12}{section.3.6}
\contentsline {section}{\numberline {3.7}\textit {Class} XMIMultiplicity}{13}{section.3.7}
\contentsline {section}{\numberline {3.8}\textit {Class} XMIAssociation}{14}{section.3.8}
\contentsline {chapter}{\numberline {4}\uppercase {Hasil}}{16}{chapter.4}
\contentsline {chapter}{\numberline {5}\uppercase {Kesimpulan}}{18}{chapter.5}
\contentsline {chapter}{Daftar Referensi}{19}{chapter.5}
\contentsline {chapter}{Lampiran 1}{20}{section*.6}
\contentsline {chapter}{Lampiran 2}{22}{section*.8}
\contentsline {chapter}{Lampiran 3}{23}{section*.11}
\contentsline {chapter}{Lampiran 4}{25}{section*.13}
\contentsline {chapter}{Lampiran 5}{26}{section*.16}
\contentsline {chapter}{Lampiran 6}{28}{section*.18}
\contentsline {chapter}{Lampiran 7}{29}{section*.21}
\contentsline {chapter}{Lampiran 8}{31}{section*.23}
\contentsline {chapter}{Lampiran 9}{32}{section*.26}
\contentsline {chapter}{Lampiran 10}{34}{section*.28}
\contentsline {chapter}{Lampiran 11}{35}{section*.31}
\contentsline {chapter}{Lampiran 12}{36}{section*.33}
\contentsline {chapter}{Lampiran 13}{37}{section*.36}
\contentsline {chapter}{Lampiran 14}{38}{section*.39}
\contentsline {chapter}{Lampiran 15}{39}{section*.41}
